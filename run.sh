#!/bin/bash

set -o errexit

# these were set in Dockerfile
#DCKR_USERREF=/input
#OUTPUT=/output
#CONT_PAIRED_FASTQ_FILE_LISTING=/input/CAMI.interleaved.fq.gz.list
#CONT_CONTIGS_FILE_LISTING=/input/CAMI.assembly.list
#CONT_BINNING_FILE=/output/metabat.binning
#BINNING_FILE_BASE=metabat.binning

TASK=$1

if [[ -z $TASK ]]; then 
    echo "Usage: $0 <task name in Taskfile> "
    exit 1
fi

# verify we have input dir and prerequisite files
if [[ ! -d $DCKR_USERREF ]]; then
    echo "No input directory found: $DCKR_USERREF"
    echo "Did you use --volume=<your/input/path>:</input>:ro when running docker image?"
    exit 1

	if [[ ! -s $CONT_PAIRED_FASTQ_FILE_LISTING ]]; then
		echo "Missing or empty file: $CONT_PAIRED_FASTQ_FILE_LISTING"
		exit 1
	fi

	if [[ ! -s $CONT_CONTIGS_FILE_LISTING ]]; then
		echo "Missing or empty file: $CONT_CONTIGS_FILE_LISTING"
		exit 1
	fi


fi

# create all temp files in a temp dir 
TMP=$(mktemp -d)
if [[ ! -d $TMP ]]; then
    echo "No temp dir created for working dir: $TMP"
    exit 1
fi
cd $TMP

READS=$(cat $CONT_PAIRED_FASTQ_FILE_LISTING)
ASSEMBLY=$(cat $CONT_CONTIGS_FILE_LISTING)

# Run the given task
CMD=$(egrep ^${TASK}: /Taskfile | cut -f 2 -d ':')
if [[ -z ${CMD} ]]; then
  echo "Abort, no task found for '${TASK}'."
  exit 1
fi

echo "eval ${CMD}"
echo assembly: $ASSEMBLY
echo reads: $READS
eval ${CMD}

# create binning file: tab delineated file mapping
# contigs-ids to bin-ids
createBinningFile.sh $TMP
if [[ ! -s "$TMP/$BINNING_FILE_BASE" ]]; then
	echo "Missing or empty file: $TMP/$BINNING_FILE_BASE"
	exit 1
fi
mv $TMP/$BINNING_FILE_BASE $OUTPUT 

# move fasta bins to output dir
RESULTS=$(ls $TMP/*.metabat-bins-.[0-9]*.fa)
if [[ -n $RESULTS ]]; then
    mv $TMP/*.metabat-bins-.[0-9]*.fa ${OUTPUT}
else
    echo Failed to find assembly output contigs
fi

# cleanup
rm -rf $TMP


