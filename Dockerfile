FROM cami/interface
MAINTAINER Jeff Froula,jlfroula@lbl.gov

# Required files
COPY dockermount.conf /dckr/etc/
COPY list-spec.sh /dckr/etc/tasks.d/--list-spec
COPY print_version.sh /dckr/etc/tasks.d/--version

# Constants
ENV DCKR_USERREF /input
ENV OUTPUT /output
ENV CONT_PAIRED_FASTQ_FILE_LISTING /input/CAMI.interleaved.fq.gz.list
ENV CONT_CONTIGS_FILE_LISTING /input/CAMI.assembly.list
ENV CONT_BINNING_FILE $OUTPUT/metabat.binning
ENV BINNING_FILE_BASE metabat.binning

#install basic requirements
RUN apt-get update -y
RUN apt-get install -y g++ make libboost-all-dev python libncurses5-dev wget scons curl realpath bzip2 zlib1g-dev

#install bbmap
RUN apt-get install -y default-jre
RUN wget --no-check-certificate http://downloads.sourceforge.net/project/bbmap/BBMap_34.94.tar.gz && \
tar -zxvf BBMap_34.94.tar.gz
RUN rm BBMap_34.94.tar.gz
RUN mv /bbmap /usr/local 
ENV PATH /usr/local/bbmap:$PATH 

#install metabat
RUN wget https://bitbucket.org/berkeleylab/metabat/get/master.tar.gz && \
tar xzvf master.tar.gz
RUN rm master.tar.gz
RUN cd berkeleylab-metabat-* && \
scons install 
# samtools should be installed with metabat
RUN mv berkeleylab-metabat-*/samtools-1.2/samtools /usr/local/bin

# add schema, tasks, run scripts
ADD metabat_wrapper.sh /usr/local/bin/metabat_wrapper.sh
ADD run.sh /usr/local/bin/run.sh
RUN chmod a+x /usr/local/bin/run.sh 
ADD Taskfile /
ADD createBinningFile.sh /usr/local/bin/

ENTRYPOINT ["/usr/local/bin/run.sh"]
