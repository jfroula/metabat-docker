#!/bin/bash
DATA=$1
if [[ -z ${DATA} ]]; then
    echo "Usage: $0 <contig dir>"
    exit 1
fi
FILES=$(ls $DATA/*.metabat-bins-.[0-9]*.fa)
FILES=($FILES)
if [[ -z ${FILES} ]]; then
    echo "Files missing or empty: $FILES"
    exit 1
fi
FILENAME=metabat.binning
SAMPLEID=CAMI_low

echo "@Version:0.9.0" > $FILENAME
echo "@SampleID:$SAMPLEID" >> $FILENAME
echo -e "@@SEQUENCEID\tBINID" >> $FILENAME

for bin in ${FILES[@]}; do  
    basebin=$(basename $bin)
    contigs=$(grep "^>" $bin|sed 's/>//')
    for c in ${contigs[@]}; do 
        echo -e "$c\t$basebin" >> $FILENAME
    done
done
