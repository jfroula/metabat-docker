#!/bin/bash
# Create metabat bins from assembly and fastq reads.
# It runs alignment to create bam files which 
# are then passed to metabat
#
# WARNING: for CAMI reads, the memory that
# bbmap requires is hard coded until we get a 
# better way.
#
# run bbmap in default mode

copy=assembly_copy.fasta

assembly=$1
shift 1
reads=("$@")
if [[ -z $assembly ]] && [[ -z $reads ]]; then
    echo "Usage: $0 <assembly> <fastq reads> <<reads2...>>"
    echo "threads is set to 2 for bbmap.sh"
    exit 1
fi

# metabat doesn't like gzipped assemblies
if [[ $assembly == *.gz ]]; then
    cp $assembly $copy.gz
    gunzip -f $copy.gz
    assembly=$copy
    
fi

# dont re-use previous depth file from metabat
rm *depth*
rm -r *ref*

#
# Run bbmap.sh
# uses all available threads on machine
#
for fastq in ${reads[@]}; do
    output=${fastq##*/}.sam
    echo "bbmap.sh -Xmx50g in=$fastq out=$output ref=$assembly"
          bbmap.sh -Xmx50g in=$fastq out=$output ref=$assembly
    if [[ -z $output ]]; then
	echo "Failed: no $output file found"
	exit 1
    fi    
    echo "samtools view -F 0x04 -uS $output | samtools sort - $output.sorted; samtools index $output.sorted.bam"
          samtools view -F 0x04 -uS $output | samtools sort - $output.sorted; samtools index $output.sorted.bam
    if [[ -z "$output.sorted.bam" ]]; then
	echo "Failed: no $output.sorted.bam file found"
	exit 1
    fi    
done

echo "runMetaBat.sh --veryspecific $assembly *.bam"
      runMetaBat.sh --veryspecific $assembly *.bam

