# README #

### What is this repository for? ###

* **metabat-docker** is a docker wrapper for MetaBAT.  MetaBAT will bin contigs of a metagenome assembly in an attempt to recreate genomes. The binning algorithm uses tetra-nucleotide frequencies(TNF) and read abundance variations to bin the contigs. It's preferable but not necessary to have multiple samples, i.e DNA samples collected and sequenced from different time series. Read abundance variation should help bin contigs derived from the same species, for example, when several contigs have roughly the same read coverage in any given sample but have different coverages between the multiple samples. The TNF signatures for these same several contigs should also be similar. When only one sample is available, TNF can still produce some bins.

* input: 1) one or more fastq files, 2) pan metagenome assembly (i.e. assembled from all samples)

* more information about MetaBAT is found here https://bitbucket.org/berkeleylab/metabat

### How do I get set up? ###

1. create `input_data` and `output_data`
2. copy fastq reads [.gz|.fq|.fastq] into `input_data`
3. copy assembly fasta into `input_data`
4. create `CAMI.interleaved.fq.gz.list` and include full paths (*within docker file structure*) to all read fastq files.
5. create `CAMI.assembly.list` and include full path (*within docker file structure*) to assembly.
6. `docker pull jfroula/metabat-docker` (you may have to login to see image: `docker login`)
7. `docker run --rm --volume=$(pwd)/input_data:/input:ro --volume=$(pwd)/output_data:/output:rw jfroula/metabat-docker default`

### Who do I talk to? ###

* jeff froula <jlfroula@lbl.gov>